const fetch = require('node-fetch');
const config = require('../config.json');
const axios = require('axios');

const SERVICE_URL = config.external_pinning_service.api_url;
const KEY_TOKEN = process.env.BUCKET_KEY;

class PinningService {
    constructor() { }

    async pinCID(cid, newCid) {
        const pinID = await this.getPinByName(cid);

        try {
            return await fetch(`${SERVICE_URL}/pins/${pinID || ''}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${KEY_TOKEN}`
                },
                body:
                    JSON.stringify({ cid: newCid })
            });
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    async unpinCID(cid) {
        const pinID = await this.getPinByName(cid);
        
        return await fetch(`${SERVICE_URL}/pins/${pinID}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${KEY_TOKEN}`
            }
        });
    }

    async getPinByName(name) {
        const body = { name };
        const config = {
            headers: { Authorization: `Bearer ${KEY_TOKEN}` }
        };

        const response = await axios.get(`${SERVICE_URL}/pins`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${KEY_TOKEN}`
            },
            data: {
                name
            }
        });

        return response.data.count ? response.data.results[0].requestid : '';
    }

}

module.exports = PinningService;