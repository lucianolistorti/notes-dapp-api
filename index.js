const express = require('express');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const config = require('./config.json');
const cors = require('cors');
const PORT = config.port;
const PINNING_API_URL = config.pinning_api_url;

if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}
const pinningRouter = require('./routes/PinningController.js');

let app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(cors());

//Define routes.
app.use(PINNING_API_URL, pinningRouter);

app.listen(PORT, () => {
    console.log(`Note dApp Server listening on port ${PORT}`)
});