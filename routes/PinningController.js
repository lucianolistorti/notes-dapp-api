const express = require('express');
const PinningService = require('../services/PinnigService.js');

const router = express.Router();
const pinningService = new PinningService();

//Pin a new CID.
router.post('/', async (req, res) => {
    const cid = req.body.cid;

    if (!cid) {
        console.error("Error in the pin request. A CID should be sent in the request body");
        return res.status(400).send({
            error: "A CID should be sent in the request body"
        });
    }

    try {

        const pinServiceResponse = await pinningService.pinCID('', cid);
        console.log(`The CID ${cid} was succesfully pinned`);

        res.status(200).send(pinServiceResponse);
    }
    catch (e) {
        console.error("Error in the pin request: ", e);
        return res.status(500).send({
            error: e
        });
    }

});

// Replace CID with new CID
router.post('/:cid', async (req, res) => {
    const cid = req.params.cid;
    const newCid = req.body.cid;

    if (!cid) {
        console.error("Error in the replace request. The CID to be replaced should be sent a URL parameter")
        return res.status(400).send({
            error: "The CID to be replaced should be sent a URL parameter"
        });
    }

    if (!newCid) {
        console.error("Error in the replace request. The new CID to be replaced should be sent a URL parameter")
        return res.status(400).send({
            error: "The new CID should be sent in the request body"
        });
    }

    try {
        const pinServiceResponse = await pinningService.pinCID(cid, newCid);
        console.log(`The CID ${cid} was replaced by new CID ${newCid}`);
        res.status(200).send(pinServiceResponse);
    }
    catch (e) {
        console.error("Error in the replace request: ", e);
        return res.status(500).send({
            error: e
        });
    }

});

// Unpin a CID
router.delete('/:cid', async (req, res) => {
    const cid = req.params.cid;

    if (!cid) {
        console.error("Error in the unpin request. A CID should be sent in the request body");
        return res.status(400).send({
            error: "A CID should be sent in the request body"
        });
    }

    try {
        const pinServiceResponse = await pinningService.unpinCID(cid);
        console.log(`The CID ${cid} was succesfully unpinned`);
        res.status(200).send(pinServiceResponse);
    }
    catch (e) {
        console.error("Error in the unpin request: ", e);
        return res.status(500).send({
            error: e
        });
    }

});

module.exports = router;